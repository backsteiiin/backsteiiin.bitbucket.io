function randInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function shuffle(array) {
		let currentIndex = array.length, temporaryValue, randomIndex;
		while (0 !== currentIndex) {
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
		}
		return array;
}

class Matrix2 {
		constructor() {
				this.matrix = [[randInt(0, 10), randInt(0, 10)], [randInt(0, 10), randInt(0, 10)]];
		}

		get det() {
				return Math.floor((this.matrix[0][0] * this.matrix[1][1]) - (this.matrix[0][1] * this.matrix[1][0]));
		}

		get cells() {
				return this.matrix[0].concat(this.matrix[1]);
		}

		get choices() {
				return shuffle([
						this.det,
						this.det - randInt(-5, 0),
						this.det - randInt(-5, 0),
						this.det - randInt(-5, 0)
				]);
		}
}

class Game {
		static score = 0;

		constructor(firstRun) {
				if (!firstRun) setTimeout(Game.main, 1000); else Game.main();
		}

		static guess(id, guess, answer) {
				let button = document.getElementById(id);
				button.onclick = function () {
						if (answer === guess) {
								let element = document.getElementById("status");
								element.innerHTML = "TRIVIAL.";
								Game.score += 1;
								document.getElementById("streak").innerHTML = "" + Game.score;
								new Game();
						}
				}
		}


		static main() {
				let element = document.getElementById("status");
				element.innerHTML = "";
				let matrix = new Matrix2();
				let choices = matrix.choices;
				const elems = ["x0y0", "x0y1", "x1y0", "x1y1"];
				for (let i = 0; i < matrix.cells.length; i++) {
						document.getElementById(elems[i]).innerText = matrix.cells[i];
				}
				for (let i = 0; i < choices.length; i++) {
						let element = document.getElementById("choice" + i);
						element.innerHTML = choices[i];
						Game.guess("btn" + i, choices[i], matrix.det);
				}

		}
}

document.getElementById("streak").innerHTML = "0";
new Game(true);
