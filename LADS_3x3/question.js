function randInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

class Matrix3 {
    min = -7;
    max = 7;

    constructor() {
        let tempDet = 10000000;
        while (tempDet > 50 || -50 > tempDet) {
            this.matrix = [
                [randInt(this.min, this.max), randInt(this.min, this.max), randInt(this.min, this.max)],
                [randInt(this.min, this.max), randInt(this.min, this.max), randInt(this.min, this.max)],
                [randInt(this.min, this.max), randInt(this.min, this.max), randInt(this.min, this.max)]];
            tempDet = this.det;
        }
    }

    get det() {
        return Math.floor(
            this.matrix[0][0] * this.matrix[1][1] * this.matrix[2][2] +
            this.matrix[0][1] * this.matrix[1][2] * this.matrix[2][0] +
            this.matrix[0][2] * this.matrix[1][0] * this.matrix[2][1] -
            this.matrix[2][0] * this.matrix[1][1] * this.matrix[0][2] -
            this.matrix[2][1] * this.matrix[1][2] * this.matrix[0][0] -
            this.matrix[2][2] * this.matrix[1][0] * this.matrix[0][1]
        );
    }

    get cells() {
        return this.matrix[0].concat(this.matrix[1].concat(this.matrix[2]));
    }

    get choices() {
        return shuffle([
            this.det,
            this.det - Math.floor(randInt(this.min / 2, this.max / 2)),
            this.det - Math.floor(randInt(this.min / 2, this.max / 2)),
            this.det - Math.floor(randInt(this.min / 2 - 5, this.max / 2)),
            this.det - Math.floor(randInt(this.min / 2, this.max / 2)),
            this.det - Math.floor(randInt(this.min / 2 - 5, this.max / 2))
        ]);
    }
}


class Game {
    static score = 0;

    constructor(firstRun) {
        if (!firstRun) setTimeout(Game.main, 1000); else Game.main();
    }

    static guess(id, guess, answer) {
        let button = document.getElementById(id);
        button.onclick = function () {
            if (answer === guess) {
                const word = "TRIVIAL";
                let element = document.getElementById("status");
                element.innerHTML += word;
                Game.score += 1;
                document.getElementById("streak").innerHTML = "" + Game.score;
                new Game();
            }
        }
    }


    static main() {
        let element = document.getElementById("status");
        element.innerHTML = "";
        let matrix = new Matrix3();
        let choices = matrix.choices;
        const elems = ["x0y0", "x0y1", "x0y2", "x1y0", "x1y1", "x1y2", "x2y0", "x2y1", "x2y2"];
        for (let i = 0; i < matrix.cells.length; i++) document.getElementById(elems[i]).innerText = matrix.cells[i];
        for (let i = 0; i < choices.length; i++) {
            let element = document.getElementById("choice" + i);
            element.innerHTML = choices[i];
            Game.guess("btn" + i, choices[i], matrix.det);
        }

    }
}

new Game(true);
